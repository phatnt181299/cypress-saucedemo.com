class saucedemo_login {
    navigate(){
        cy.visit('https://saucedemo.com/')
    }
    // Type username
    enter_username_textbox(username) {
        cy.get('#user-name')
          .type(username);
        return this
    }
    // assert username
    assert_username(username){
        cy.get('#user-name')
        .should('have.value', username);
    }
    // Type password
    enter_password_textbook(password){
        cy.get('#password')
          .type(password);
        return this
    }
    // Click login button
    click_login_button(){
        cy.contains('Login').click();
    }
    // Assert that locked account can not access to the web
    assert_lockout_page(){
        cy.url().should('eq','https://www.saucedemo.com/')
        cy.get('div.error-message-container.error')
          .should('have.text', 'Epic sadface: Sorry, this user has been locked out.')
    }
}
export default saucedemo_login