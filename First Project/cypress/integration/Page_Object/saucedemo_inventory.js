class saucedemo_inventory{
    navigate(){
        cy.visit('https://www.saucedemo.com/inventory.html')
    }
    // Assert item_4_title_link = "Sauce Labs Backpack"
    assert_item_4(locator,text){
        cy.get(locator)
        .should('have.text', text)
    }
    // Print out tilte of 5(page contains 5 item) product
    printout_all_item(val){
        let validate = val
        for(let i=0; i <= validate; i++){
        cy.get('#item_'+i+'_title_link').invoke('text').then(cy.log)
    }
  }
}
export default saucedemo_inventory