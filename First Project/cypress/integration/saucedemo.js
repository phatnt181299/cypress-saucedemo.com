import saucedemo_login from '../integration/Page_Object/saucedemo_login.js';
import saucedemo_inventory from '../integration/Page_Object/saucedemo_inventory';
const login = new saucedemo_login()
const invent = new saucedemo_inventory()

let standard_username = 'standard_user'
let locked_out_user = 'locked_out_user'
let password = 'secret_sauce'

context('Actions', () => {
    beforeEach(() => {
        login.navigate()
      })
    
    it('Login Page', () => {
        login.enter_username_textbox(standard_username)
        login.assert_username(standard_username)
        login.enter_password_textbook(password)
        login.click_login_button()
    })

    it('Assert Product - 1', () =>{
        login.enter_username_textbox(standard_username)
        login.assert_username(standard_username)
        login.enter_password_textbook(password)
        login.click_login_button()
      
      // Assert item_4_title_link = "Sauce Labs Backpack" 
        let locator = '#item_4_title_link'
        let text = 'Sauce Labs Backpack' 
        invent.assert_item_4(locator,text)
      // Print out tilte of 5(page contains 5 item) product
        invent.printout_all_item(5)
    })

    it('Assert lockout user - 2', () =>{
      login.enter_username_textbox(locked_out_user)
      login.assert_username(locked_out_user)
      login.enter_password_textbook(password)
      login.click_login_button()
      login.assert_lockout_page()
    })

})